<?php
?>
<body>
<head></head>
<style>
body{
    background-image: url("assets/images/back.jpg");
    background-color: #eee;
}
#login_form{
    max-width:550px;
    /* background: #ffeeef; */
    border: 1px solid transparent;
    /* border-radius: 2px; */
    margin-top:10%;
    padding:3%;
    box-shadow:0px  0px 5px grey;  
    color:rgba(120, 123, 158, 1);
}
input,button{
    padding:1%;
    margin:3%;
    box-shadow:0px  0px 5px grey;
    border:none;  
}
</style>
    
<center >

<div id="login_form" class="col s12">
    <h5>Login as</h5>
        <div>

            <label><input class="with-gap" name="roll" value="1" type="radio"  checked /><span>Admin</span></label>

            <label><input class="with-gap" name="roll" value="2" type="radio"  /><span>Teacher</span></label>

            <label><input class="with-gap" name="roll" value="3" type="radio"  /><span>Parent</span></label>

            <label><input class="with-gap" name="roll" value="4" type="radio"  /><span>Student</span></label>

        </div>


        <input class="input-field" type="text" id="identity" placeholder="Email or Phone"> <br>
        <input class="input-field" type="password" id="password" placeholder="Password"> <br>

    <button class="waves-effect waves-light btn" id="login_submit" >Submit  </button>

</div>

</center>

<script>

$("#login_submit").click(function(){

        var tester = '';

        tester += ($('#identity').val() == '') ? 'Please provide username. ': '';
        tester += ($('#password').val() == '') ? 'Please provide password. ': '';

        var  roll =  $('input[name=roll]:checked').val();


    if(tester == ''){
        $.ajax({
                url: 'db.php', type: 'POST',
                data: {'identity':$('#identity').val(),'password': $('#password').val(),'roll': roll,'auth': true},
                success: function (response) {
                        location.reload();
                    }
                ,error: function (error) {console.log(error);}
        });
    }else{
        // alert(tester);
    }

});

</script>

