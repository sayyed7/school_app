<?php
// print_r($_SESSION['user']);die;
$admin_links = ["class"=>"Classes","teacher"=>"Teachers","student"=>"Students","forms_and_slips"=>"Forms and Slips"];
$student_links = ["stud_time_table"=>"My Timetable","stud_home_work"=>"Home work","stud_fees"=>"My Fees Details","stud_attendance"=>"My Attendance"];
$teacher_links = ["teacher_attendance"=>"Attendance","teacher_assignment"=>"Assignment","leave_applications"=>"Leave Applications","teacher_attendance"=>"Attendance"];
$parent_links = ["stud_time_table"=>"Timetable","stud_home_work"=>"Home work","stud_fees"=>"Leave Applications","stud_attendance"=>"Attendance"];
?>
<style>
#top_bar{background-color: #0cbcd4;text-align: right;box-shadow: 0px 1px 2px #888888;min-height:50px}
section{
  /* outline:red groove thin; */
  max-width:76%;
  margin-left:23%;
  margin-top:1%;
  min-height:87%;
  background-color:#ffffef;
  box-shadow: 0px 1px 2px #888888;
}
</style>
<ul id="nav-mobile" class="sidenav sidenav-fixed" >

<li><div class="user-view">
  <div class="background">
    <img src="assets/images/profile_back.jpg">
  </div>
  <a href="#user"><img class="circle" src="assets/images/admin.png"></a>
  <a href="#name"><span class="white-text name"><?php echo $_SESSION['user']['name'].' '.$_SESSION['user']['last_name'] ; ?></span></a>
  <a href="#email"><span class="white-text email">Welcome To Dashboard</span></a>
</div></li>
</li>
<?php 
$nav_lis = [];
($_SESSION['user']['roll'] == '1') ? $nav_lis = $admin_links   : FALSE;
($_SESSION['user']['roll'] == '2') ? $nav_lis = $student_links : FALSE;
($_SESSION['user']['roll'] == '3') ? $nav_lis = $teacher_links  : FALSE;
($_SESSION['user']['roll'] == '4') ? $nav_lis = $parent_links : FALSE;
foreach ($nav_lis as $k => $v) {
  echo "<li class='bold'><a href='$k' class='waves-effect waves-teal nav_lis'>$v</a></li>";
}
?>
</ul>
<div id="top_bar">
<ul id="slide-out" class="side-nav fixed">
  <a href="#" style="font-size:150%;float:left" data-target="nav-mobile" class="top-nav sidenav-trigger full hide-on-large-only"><b class="material-icons">&#x2630;</b></a>
</ul>
 <a href=""> <span id="logout"  >Logout</span></a>
</div>

<section></section>
<script>
$(document).ready(function(){
  $('.sidenav').sidenav();
});

$("#logout").click(function(){  $.ajax({url: 'db.php', type: 'POST',data: {'logout': true},
          success: function (response) {location.reload();},error: function (error) {console.log(error);}});
});

$("a.nav_lis").click(function(){
          event.preventDefault();
          var page =    "sections/"+$(this).attr('href')+".html";
          $("section").html('<center><img src="assets/images/load.gif" alt=""></center>');
          $.ajax({
                  url: 'db.php', type: 'POST',
                  data: {'page':page,'get_view': true},
                  success: function (response) {
                          // console.log(response);
                          $("section").html(response);
                      }
                  ,error: function (error) {console.log(error);}
          });

});


</script>