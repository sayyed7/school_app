<?php
session_start();
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <script src="assets/js/0_jquery.js"></script>
    <script src="assets/js/materialize.js"></script>
    <link type="text/css" rel="stylesheet" href="assets/css/materialize.css"  media="screen,projection"/>
</head>
<style>
body{
    font-family: "Trebuchet MS", Helvetica, sans-serif	;
    background-color: #fbfbfb;
}
</style>

<?php
if(isset($_SESSION['login'])){
    require __DIR__ . '/views/dashboard.php';
}else{
    require __DIR__ . '/views/login.php';
}
?>
</html>